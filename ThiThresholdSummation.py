'''
Created on Dec 2, 2013

@author: tim.anderson


THI Threshold Summation takes all 3 models, a1b, a2, and b2, iterates over their THI tifs,
and generates two tiffs for each model. Each tif contains 100 bands, one for each year.
These tifs contain values for how many times the THI goes over a threshold per pixel in one tif,
and how many valid values were for that pixel in the other tif.
'''
from osgeo import gdal
from osgeo.osr import SpatialReference
from gdalconst import GA_ReadOnly
from os import remove, mkdir
from os.path import join as pathJoin, exists as pathExists
from numpy import zeros as EmptyNumpyArray, float32 as numpyTypeFloat32, int32 as numpyTypeInt32
from sys import exit
from multiprocessing import Pool, Process
daysThatHaveThiValues = range(145, 245) #days were agreed-upon , as only the summer days are important in these THI calculations.
ThiThreshold = 74

a1bFolders = ["miroc32_a1b", "Cgcm31_a1b_7_09","csiromk35_a1b_7_09"]
a2Folders = ["Cgcm31_a2_7_09","csiromk35_a2_7_09","miroc32_a2"]
b2Folders = ["Cgcm2_b2_12_09","Hadcm3_b2_12_09", "miroc32_a2"]
baseOutputDirectory = "Thi_Threshold_Counts"
tempFolder = "tempFiles"
templateFileLoc = pathJoin(tempFolder,"ThiThresholdTemplate.tif")


#creates a template file witjh 100 bands to be used to generate output files.
def makeTemplate(templateFileLoc):
    DS = gdal.Open(pathJoin("miroc32_a2","VPD_2080.tif"))
    if DS == None:
        print"Could not create template."
        return
    
    srs = SpatialReference()
    xSize = DS.RasterXSize
    ySize = DS.RasterYSize
    inputDriver = DS.GetDriver()
    inputTransform = DS.GetGeoTransform()
    outDS = inputDriver.Create(templateFileLoc, xSize, ySize, 100, gdal.GDT_Int32)
    outDS.SetGeoTransform(inputTransform)
    outDS.SetProjection(srs.ExportToWkt())
    DS = None
    return outDS

#takes an input THI dataset, arrays for threshold and total values for that year, and the year's filename,
#and increments the threshold and total arrays where applicable from the input Dataset.
def countFromDataset(inputDS, thresholdArray, totalArray, thiFileName):
    try:
        for day in daysThatHaveThiValues:
            #print str(thiFileName + " band "+ str(day))
            inputArray = inputDS.GetRasterBand(day).ReadAsArray()
            for row in range(len(inputArray)):
                for col in range(len(inputArray[0])):
                    pixelValue = inputArray[row][col]
                    #if this pixel passes the threshold, increment threshold count.
                    if pixelValue >=74:
                        thresholdArray[row][col] += 1
                    #if the pixel was a valid value,
                    #increment the total count for this set.
                    if pixelValue != 0.0 and pixelValue >-999: 
                        totalArray[row][col] += 1
    except IndexError:
        print "ERROR, index exception."
        print thiFileName
        print "input ds dims: " + str(inputDS.RasterXSize) + ", "+ str(inputDS.RasterYSize)
        print "day was: " + str(day)
        print "row was: " +str(row)
        print "col was: " +str(col)
        print "total array dims: " + str(len(totalArray)) + ", " + str(len(totalArray[0]))
        print "thresholdArray dims: "+ str(len(thresholdArray)) + ", " +str(len(thresholdArray[0]))
        print "input array dims: " + str(len(inputArray)) + ", " + str(len(inputArray[0]))
        print "input array:"
        print inputArray
        print "threhold Array:"
        print thresholdArray
        print "totalArray:"
        print totalArray
        exit()
		
#takes a list of folders, string modelName, a Dataset to use as a template, that template's driver,
#and the height and width of the rasters as a tuple, and generates THI threshold data and puts
#it into the output folder.
def makeThiThresholdFromFolderSet(folderSet, modelName, template, templateDriver, rasterDimensions):
    #folderSet, modelName, template, templateDriver, rasterDimensions = arglist
    arrayWidth = rasterDimensions[1]
    arrayHeight = rasterDimensions[0]
    print "starting model "+ modelName
    print "Generating empty arrays..."
    #generate 100-item arrays containing all zero numpy arrays.
    thresholdCountArrays = [EmptyNumpyArray((arrayWidth, arrayHeight), numpyTypeInt32) for x in range(100)]
    totalCountArrays = [EmptyNumpyArray((arrayWidth, arrayHeight), numpyTypeInt32) for x in range(100)]
    print "arrays generated for entire model."
    
    for folder in folderSet:
        for year in range(100):
            thiFile = pathJoin(folder,"THI_"+ str(year+2001) + ".tif")
            inputDS = gdal.Open(thiFile)
            if inputDS == None:
                print "could not open dataset " + thiFile
                continue
            print "Aggregating " + str(thiFile)
            countFromDataset(inputDS, thresholdCountArrays[year], totalCountArrays[year], thiFile)
            inputDS = None      #close the dataset

    thresholdTifName =  pathJoin(baseOutputDirectory, str(modelName + "_THIThresholdCount.TIF"))
    totalCountTifName = pathJoin(baseOutputDirectory, str(modelName + "_THITotalCount.TIF"))
    thresholdDS =  templateDriver.CreateCopy(thresholdTifName,  template, 0)
    totalCountDS = templateDriver.CreateCopy(totalCountTifName, template, 0)
    for year in range(100):
        #band is year + 1 because bands are 1 indexed instead of zero indexed.
        thresholdDS.GetRasterBand(year+1).WriteArray(thresholdCountArrays[year])
        totalCountDS.GetRasterBand(year+1).WriteArray(totalCountArrays[year])
    
    
    thresholdDS = None  #close the datasets.
    totalCountDS = None
    print "Completed aggregating data for model " + modelName + ", creating output Tifs."
    print "completed model "+ modelName

if __name__ == '__main__':
    print ("This THI Threshold calculator will sum all of the times THI values go over a threshold for each model.")
    template = None
    
    if not pathExists(baseOutputDirectory): #create output directory if needed.
        mkdir(baseOutputDirectory)
    if not pathExists(tempFolder):          #create temp directory if needed.
        mkdir(tempFolder)
    if pathExists(templateFileLoc):     #make template if not made yet, and load it into template variable.
        template = gdal.Open(templateFileLoc)
    else:
        template = makeTemplate(templateFileLoc)
    if template == None:
        print "could not open or create template, exiting now."
        exit()

    templateDriver = template.GetDriver()
    rasterDimensions= (template.RasterXSize, template.RasterYSize)
    # multiprocessing can cause access errors to the template, so these three datasets are computed synchronously.
    makeThiThresholdFromFolderSet(b2Folders, "b2", template, templateDriver, rasterDimensions)
    makeThiThresholdFromFolderSet(a2Folders, "a2", template, templateDriver, rasterDimensions)
    makeThiThresholdFromFolderSet(a1bFolders, "a1b", template, templateDriver, rasterDimensions)
    template = None # close template
