'''
Created on Nov 5, 2013

@author: tim.anderson

This python script looks through the geotiff datasets in local folders,
and reports any that have bands solely comprised of no-data or zeros.
Output is two text files, one for files with every band invalid, and
one for files with some valid bands and some invalid ones.
'''
from osgeo import gdal
from os import listdir
from os.path import isdir
from os.path import exists as pathExists
from os import mkdir
from os.path import join as pathJoin


def bandContainsGoodValue(band):
    array = band.ReadAsArray()
    for row in array:
        for ele in row:
            if ele != 0.0:
                return True
    return False

def CheckForBadValues(item):
    ds = gdal.Open(item)
    badBandList = []
    for day in range(1,366):
        
        if not bandContainsGoodValue(ds.GetRasterBand(day)):
            badBandList.append(day)
    numBadBands = len(badBandList)
    if numBadBands == 0:
        print item + " contained all valid bands"
    elif numBadBands == 365:
        print item + " had NO valid bands, the entire tiff was bad data."
        with open("noDataTiffs.txt","a+") as noDataTiffsFile:
            noDataTiffsFile.write(item + "\n")
    # item had some good bands, some bad bands.
    else:
        print item + " had some bad bands."
        with open("someBadDataTiffs.txt","a+") as someBadDataFile:
            someBadDataFile.write(item + "'s bad bands: ")
            for bandNum in badBandList:
                someBadDataFile.write(str(bandNum) + ",")
            someBadDataFile.write("\n")

if __name__ == '__main__':
    localFolders = [folder for folder in listdir(".") if isdir(folder)]
    #if there are no folders found where this script resides, let the user know and exit the program.
    if len(localFolders)== 0:
        print "no folders were found here. Is the script in the right place?"
        print "It should be one folder below wherever the geotiffs are going to be located."
        exit()
    for folder in localFolders:
        itemList = []
        for item in listdir(folder):     # get the files in the folder
            if isdir(item) or not item.endswith("tif"):
                continue                    # ignore folders and only look at tiffs
            #if the item doesn't have a '_' in the filename,
            #it can't be what we're looking for, so ignore it.
            if "_" not in item:
                print "no _"
                print item
                continue
            itemTuple = item.split("_")
            #if there are too many things in the split tuple,
            #it can't be what we're looking for, so ignore it.
            if len(itemTuple) is not 2:
                print "not 2 split"
                print item
                print
                continue
            itemList.append(pathJoin(folder,item))
        if(len(itemList)>0):
            print "found " + str(len(itemList)) + " matches in folder " + folder
        else:
            print folder + " contained no geotiffs."
            pass
        for item in itemList:
            CheckForBadValues(item)
        print "Folder Completed."
    print"program has finished, all THI geotiffs should be present in their respective folders."
    exit()