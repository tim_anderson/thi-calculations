'''
Created on Sep 30, 2013

@author: tim.anderson


THI Calculator for creating THI geotiffs from daily average temperature and vapor pressure deficit inputs.
run this python script from the directory above where all the geotiffs are located
In practice, this should be put in MapClimateData.
'''
from osgeo import gdal
from math import exp
from os import listdir, mkdir
from os.path import isdir, join as pathJoin, exists as pathExists, basename as fileBaseName
from shutil import move as fileMove
from numpy import zeros as EmptyNumpyArray, float32 as numpyTypeFloat32
from multiprocessing import Pool
OnlyDoJunJulAug = True
noDataValue = -9999.0

#vapor pressure given in kilopascals, air temp in celsius. Returns None on arithmetic error.
def CalcRh(pascals, temp):
    try:
        saturation = 0.611* exp( ( 17.502 * temp ) / ( 240.97 + temp ))
        ambientVapor = saturation - pascals
        rh = (ambientVapor / saturation)
        #cap rh so it can't go higher than 1.0 or lower than 0.0
        rh = max(rh, 0.0)
        rh = min(rh, 1.0)
        return rh
    except ArithmeticError: #return error condition if something went wrong (divison by zero)
        return None
 
def CalcTHI(vpd, temp):
        #if vpd or temp are no-data-provided values, also give one as output.
        if vpd <=-999 or temp <= -999:
            return noDataValue
        #if vpd and temp are both zeros, treat it as illegal data as well and return the same.
        if vpd == 0.0 and temp == 0.0:
            return 0.0
        #get pressure deficit into pascals for correct units.
        pascals = vpd/1000.
        rh = CalcRh(pascals,temp)
        if rh == None:
            return noDataValue
        THI = 0.8 * temp + rh *(temp-14.4) + 46.4
        return THI

#Makes the THI output file for the given set of Tday and VPD geotiffs.
def MakeTHI(itemTuple):
    tempFile, vpdFile, thiFileOutput = itemTuple
    #create a temp file name for this tif.
    temporaryFile =pathJoin("tempFiles/", fileBaseName(thiFileOutput).split('.')[0] + "tempFile.tif")
    
    
    print "starting work on " + str(thiFileOutput) + "."
    
    tempDataset     = gdal.Open(tempFile)
    vpdDataset      = gdal.Open(vpdFile)
    if tempDataset == None:
        print "Could not open file "+ str(tempFile) +"."
        print "Skipping and moving on to next fileset."
        return
    if vpdDataset == None:
        print "Could not open file "+ str(vpdFile) +"."
        print "Skipping and moving on to next fileset."
        return
    
    #put the new THI dataset into a temp file to be copied over when done.
    #thiDataset      = tempDataset.GetDriver().CreateCopy(temporaryFile, vpdDataset)
    tempDriver 		= tempDataset.GetDriver()
    thiDataset		= tempDriver.Create(temporaryFile, tempDataset.RasterXSize, tempDataset.RasterYSize, 365, gdal.GDT_Float32)
    thiDataset.SetGeoTransform(tempDataset.GetGeoTransform())
    thiDataset.SetProjection(tempDataset.GetProjection())
    
    if thiDataset == None:
        print "Could not create temp file "+ str(vpdFile) +"."
        print "Skipping and moving on to next fileset."
        return
    print "creating and opening files was sucessful, cool."
    daysToCompute = range (145,245) if OnlyDoJunJulAug else range(1, 366)
    for day in daysToCompute:
        print thiFileOutput + ", Band "+ str(day)
        vpdBand         = vpdDataset.GetRasterBand(day)
        tempBand        = tempDataset.GetRasterBand(day)
        thiBand         = thiDataset.GetRasterBand(day)
        vpdData         = vpdBand.ReadAsArray()
        tempData        = tempBand.ReadAsArray()
        thiData         = thiBand.ReadAsArray()
        #compute the THI values and put them into the ThiDataset bands. On IndexError, skip this band.
        try:
            for row in range(len(vpdData)):
                for col in range(len(vpdData[row])):
                    thi = CalcTHI(vpdData[row,col], tempData[row,col])
                    thiData[row,col] = thi
            thiBand = thiDataset.GetRasterBand(day)
            thiBand.WriteArray(thiData)
            thiBand.FlushCache()
        except IndexError:
            print "Index Error has occured."
            #fill this data band with an empty array of zeros.
            thiDataset.GetRasterBand(day).WriteArray(\
                                            EmptyNumpyArray( (len(vpdData), len(vpdData[0]) ),\
                                            numpyTypeFloat32))
    
    
    #close and finalize all of the datasets.
    print "created dataset " + thiFileOutput
    tempDataset     = None
    vpdDataset      = None
    thiDataset      = None
    #move the tempfile to it's proper place to show it is done.
    fileMove(temporaryFile, thiFileOutput)


#debug printing to check values.
def printDataValues (temperatureData, vpdData, thiData):
    for row in range(len(temperatureData)):
        for col in range(len(temperatureData[row])):
            temp = temperatureData[row,][col]
            vpd = vpdData[row][col]
            thi = thiData[row][col]
            print ((temp, vpd, thi, CalcTHI(vpd, thi) ))


if __name__ == '__main__':
    print "This program will look in all of the directories in the same location as it, and create THI geotiffs from VPD and daily temp data."
    
    
    localFolders = [folder for folder in listdir(".") if isdir(folder)]
    #if there are no folders found where this script resides, let the user know and exit the program.
    if len(localFolders)== 0:
        print "no folders were found here. Is the script in the right place?"
        print "It should be one folder below wherever the geotiffs are going to be located."
        exit()
    #create the temp folder
    if not pathExists("tempFiles/"):
        mkdir("tempFiles/")
    for folder in localFolders:
        itemList = []
        for item in listdir(folder):     # get the files in the folder
            if isdir(item) or not item.endswith("tif"):
                continue                    # ignore folders and only look at tiffs
            #if the item doesn't have a '_' in the filename,
            #it can't be what we're looking for, so ignore it.
            if "_" not in item:
                continue
            itemTuple = item.split("_")
            #if there are too many things in the split tuple,
            #it can't be what we're looking for, so ignore it.
            if len(itemTuple) is not 2:
                continue
            tiffType, year = item.split("_")
            if tiffType == "Tday":
                TdayLoc = pathJoin(folder, item)
                matchingVPD = pathJoin(folder,"VPD_" + year)
                matchingTHI = pathJoin(folder,"THI_" + year)
                if(pathExists(matchingVPD)) and not pathExists(matchingTHI):
                    itemList.append((TdayLoc, matchingVPD,matchingTHI))
        if(len(itemList)>0):
            print "found " + str(len(itemList)) + " matches in folder " + folder
        else:
            print folder + " had no geotiffs that needed processing."
            pass
        pool = Pool(processes = 3)
        pool.map(MakeTHI, itemList)
#         for item in itemList:
#             MakeTHI(item)
        print "Folder Completed."
    print"program has finished, all THI geotiffs should be present in their respective folders."
    exit()
